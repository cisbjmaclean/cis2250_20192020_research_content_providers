package info.hccis.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button addContact = (Button) findViewById(R.id.button);
        addContact.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //This is the action of adding a contact.
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
        //This puts the information within the contact, this is basically programmatically filling it out.
        intent.putExtra(ContactsContract.Intents.Insert.NAME, "Tristan");
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, "9029991234");
        //This references the common data types in which we can use the systems phone type.
        intent.putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, "johnny@hollandcollege.com");
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK);
        //This than starts the process.
        startActivity(intent);


    }
}
